defmodule Standard do
  # @spec list?(term()) :: boolean()

  # Import Kernel Methods
  import __MODULE__.Core

  require __MODULE__.Extension
  # use __MODULE__.Core, async: true

  begin "Module Documents" do
    @moduledoc """
    Documentation for `Standard`.
    """
  end

  begin "Some Alias Guard" do
    @spec list?(any) :: {:__block__ | {:., [], [:erlang | :is_list, ...]}, [], [...]}
    defguard list?(term) when is_list(term)

    @spec tuple?(any) :: {:__block__ | {:., [], [:erlang | :is_tuple, ...]}, [], [...]}
    defguard tuple?(term) when is_tuple(term)

    @spec atom?(any) :: {:__block__ | {:., [], [:erlang | :is_atom, ...]}, [], [...]}
    defguard atom?(term) when is_atom(term)

    @spec binary?(any) :: {:__block__ | {:., [], [:erlang | :is_binary, ...]}, [], [...]}
    defguard binary?(term) when is_binary(term)

    @spec string?(any) :: {:__block__ | {:., [], [:erlang | :is_binary, ...]}, [], [...]}
    defguard string?(term) when is_binary(term)

    @spec bool?(any) :: {:__block__ | {:., [], [:erlang | :is_boolean, ...]}, [], [...]}
    defguard bool?(term) when is_boolean(term)

    @spec boolean?(any) :: {:__block__ | {:., [], [:erlang | :is_boolean, ...]}, [], [...]}
    defguard boolean?(term) when is_boolean(term)

    @spec nil?(any) ::
            {:__block__ | {:., [{any, any}, ...], [:== | :erlang, ...]},
             [{:context, Kernel} | {:import, Kernel}], [...]}
    defguard nil?(term) when is_nil(term)

    @spec may_char?(any) ::
            {:__block__ | {:., [], [:andalso | :erlang, ...]}, [],
             [{:= | {any, any, any}, [], [...]}, ...]}
    defguard may_char?(term)
             when is_list(term) and is_integer(hd(term)) and hd(term) >= 7 and hd(term) <= 126
  end

  begin "Mixin Core Methods" do
    @spec begin([{:do, any}, ...]) :: any
    defmacro begin(do: block), do: __MODULE__.Core.begin(do: block)

    @spec begin(any, [{:do, any}, ...]) :: any
    defmacro begin(_commit, do: block), do: __MODULE__.Core.begin(do: block)

    @spec return(any) :: any
    defmacro return(block), do: __MODULE__.Core.return(block)

    left do
      defmacro return(block, pattern) do
        quote bind_quoted: [value: block, pattern: pattern] do
          case value do
            pattern ->
              result
          end
        end
      end
    end

    use __MODULE__.Core, type: :pipe

    use __MODULE__.Core, type: :left
  end

  begin "Macro Defination" do
    begin :C do
      begin {:check, 3} do
        defmacro check(condition, if_true, if_false) do
          quote do
            case unquote(condition) do
              bool when bool == nil or bool == false ->
                unquote(if_false)
              _ ->
                unquote(if_true)
            end
          end
        end
      end
    end
  end

  @spec hello :: :world
  @doc """
  Hello world.

  ## Examples

      iex> Standard.hello()
      :world

  """
  def hello do
    :world
  end

  left do
    defmacro pipe(list) do
      [{h, _} | t] =
        Enum.reduce(list, fn last, now ->
          {:|>, [], [now, last]}
        end)
        |> Macro.unpipe()

      fun = fn
        {{num, x}, _}, acc when is_integer(num) ->
          Macro.pipe(acc, x, num - 1)

        {x, pos}, acc ->
          Macro.pipe(acc, x, pos)
      end

      :lists.foldl(fun, h, t)
    end
  end

  defmacro nyi(info) do
    quote do
      # file = unquote(__ENV__.line)
      # module = unquote(__MODULE__)
      # line = unquote(__ENV__.line)
      IO.puts(
        "#{unquote(IO.ANSI.red())}*** (NotYetImplemented) #{Path.basename(__ENV__.file)}:#{
          __ENV__.line
        }: #{inspect(unquote(info))} in #{Regex.replace(~r/^nil$/, inspect(__MODULE__), "root")}#{
          unquote(IO.ANSI.normal())
        }"
      )

      exit(:nyi)
    end
  end

  defmacro extends(module) do
    module = Macro.expand(module, __CALLER__)
    functions = module.__info__(:functions)
    signatures = Enum.map functions, fn { name, arity } ->
      args = if arity == 0 do
               []
             else
               Enum.map 1 .. arity, fn(i) ->
                 { String.to_atom(<< ?x, ?A + i - 1 >>), [], nil }
               end
             end
      { name, [], args }
    end

    zipped = List.zip([signatures, functions])
    for sig_func <- zipped do
      quote do
        defdelegate unquote(elem(sig_func, 0)), to: unquote(module)
        defoverridable unquote([elem(sig_func, 1)])
      end
    end
  end
end

defmodule Standard.Core do
  #
  #
  #

  # {begin, 1}
  @spec begin([{:do, any}, ...]) :: any
  defmacro begin(do: block), do: block
  @spec begin(any, [{:do, any}, ...]) :: any
  defmacro begin(_commit, do: block), do: block

  @spec return(any) :: any
  defmacro return(block), do: block

  defmacro left(do: _), do: nil

  @spec pipe([any]) :: any
  defmacro pipe(list) do
    [{h, _} | t] =
      Enum.reduce(list, fn last, now ->
        {:|>, [], [now, last]}
      end)
      |> Macro.unpipe()

    fun = fn
      {{num, x}, _}, acc when is_integer(num) ->
        Macro.pipe(acc, x, num - 1)

      {x, pos}, acc ->
        Macro.pipe(acc, x, pos)
    end

    :lists.foldl(fun, h, t)
  end

  @spec pipe(any, any) :: any
  defmacro pipe(left, right) do
    [{h, _} | t] = Macro.unpipe({:|>, [], [left, right]})

    fun = fn
      {{num, x}, _}, acc when is_integer(num) ->
        Macro.pipe(acc, x, num - 1)

      {x, pos}, acc ->
        Macro.pipe(acc, x, pos)
    end

    :lists.foldl(fun, h, t)
  end

  defmacro __using__(opts) do
    sort = Keyword.get(opts, :type, nil)

    case sort do
      :pipe ->
        quote do
          defmacro pipe(left, right) do
            [{h, _} | t] = Macro.unpipe({:|>, [], [left, right]})

            fun = fn
              {{num, x}, _}, acc when is_integer(num) ->
                Macro.pipe(acc, x, num - 1)

              {x, pos}, acc ->
                Macro.pipe(acc, x, pos)
            end

            :lists.foldl(fun, h, t)
          end

          defmacro pipe(list) do
            [{h, _} | t] =
              Enum.reduce(list, fn last, now ->
                {:|>, [], [now, last]}
              end)
              |> Macro.unpipe()

            fun = fn
              {{num, x}, _}, acc when is_integer(num) ->
                Macro.pipe(acc, x, num - 1)

              {x, pos}, acc ->
                Macro.pipe(acc, x, pos)
            end

            :lists.foldl(fun, h, t)
          end
        end

      :left ->
        quote do
          defmacro left(do: _), do: nil
        end

      _ ->
        nil
    end
  end
end

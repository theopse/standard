defmodule Standard.Enum do
  import Standard

  begin :a do
    # Define Function Name

    @spec append(Enum, any) :: any
    def append(args, value)

    # Achievements

    begin :append do
      def append(args, value) when is_list(args),
        do:
          args
          |> :lists.append([value])

      def append(args, value),
        do:
          args
          |> Enum.reverse()
          |> then(&[value | &1])
          |> Enum.reverse()
    end
  end

  begin :p do
    # Define Function Name

    @spec pop(any) :: any
    def pop(list)

    # Achievements

    begin :pop do
      def pop(list) when :erlang.length(list) > 0,
        do:
          list
          |> Enum.reverse()
          |> then(fn [last | list] ->
            {last, list |> Enum.reverse()}
          end)
    end
  end
end
